var output = '',
	used = false,
	multiples = [];
	multiples[3] = 'Fizz';
	multiples[5] = 'Buzz';
//  multiples[7] = 'Flub';

Number.prototype.isMultipleOf = function(i) { return this % i === 0; };

for (var i = 1; i <= 100; i += 1) {
	used = false;
	output = '';

	multiples.forEach(function (value, index) {
		if(i.isMultipleOf(index)) {
			used = true;
			output += '<span class="' + value.toLowerCase() + '">' + value + '</span>';
		}
	});

	// If its not a multiple of anything we'll output just the number
	if(!used) {
		output += i;
	}

	document.getElementById('output').innerHTML += output + '<br>';
}